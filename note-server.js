const config = require('config');
const express = require('express');
const app = express();


const env = ['note_jwtPrivateKey', 'authy_key', 'note_twoFactorKey', 'nodemailer_key', 'SP_captchaSecretKey'];

for(let key of env) {
  if(!config.get(key)) throw new Error('Fatal error - ' + key + ' is not defined - check env variables');
}

require('./startup/logging')();
require('express-async-errors');
require('./startup/routes')(app);
require('./startup/prod')(app);
require('./startup/db')();

app.use(function (error, req, res, next) {
  if (error instanceof SyntaxError) {
    return res.status(400).send("Syntax error");
  } else {
    next();
  }
});

app.listen(5000);