const jwt = require('jsonwebtoken');
const config = require('config');
const {User} = require('../models/user');

module.exports = async function (req, res, next) {
    const token = req.header('x-auth-token');
    if(!token) return res.status(401).send('Access denied. No token provided.');

    try {
        const decoded = jwt.verify(token, config.get('note_jwtPrivateKey'));
        if (!decoded.login) return res.status(403).send("This token can't be used for that operation");
        req.user = decoded;
        const user = await User.findOne({email: decoded.email});
        if(user.passwordUpdatedOn && Date.parse(user.passwordUpdatedOn) > Date.parse(decoded.signedOn) || !decoded.signedOn)
        return res.status(400).send("This token has expired.");
        next();
    }
    catch (ex) {
        res.status(400).send('Invalid token.');
    }
}