const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    const token = req.header('x-trusted');
    req.bypass = false;
    if(!token) return next();

    try {
        const decoded = jwt.verify(token, config.get('note_twoFactorKey'));
        if(decoded.email !== req.body.email) throw new Error();
        req.bypass = true;
        next();
    }
    catch(ex){
        next();
    }
}