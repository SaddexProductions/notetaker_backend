const winston = require('winston');

module.exports = function(err, req, res, next){
    const logger = winston.createLogger({
        level: 'error',
        format: winston.format.json(),
        defaultMeta: { service: 'user-service' },
        transports: [
      
          new winston.transports.File({ filename: 'error.log', level: 'error' }),
        ]
      });
    logger.error(err.message, err);

    res.status(500).send('Something failed');
}