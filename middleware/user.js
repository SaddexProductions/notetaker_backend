const { User } = require ('../models/user');

module.exports = async function (req, res, next) {
    const user = await User.findById(req.user._id);
    if(!user) return res.status(403).send("This user doesn't exist");

    next();
}