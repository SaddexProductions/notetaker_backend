//npm modules
const express = require('express');

//local modules
const auth = require('../middleware/auth');
const { Note, validate, validateArray } = require ('../models/note');
const user = require('../middleware/user');

//setup
const router = express.Router();
router.use(auth);
router.use(user);

//routes

router.get('/', async (req, res) => {
    const notes = await Note.find({user: req.user._id});
    return res.json(notes);
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const note = new Note({
        title: req.body.title,
        tags: req.body.tags,
        content: req.body.content,
        user: req.user._id,
        date: new Date().toISOString().split('.')[0],
        edited: new Date().toISOString().split('.')[0]
    });

    await note.save();

    res.json(note);
});

router.post('/row', async (req, res) => {
    const { error } = validateArray(req.body.notes);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const arr = [];
    req.body.notes.forEach(n => {
        arr.push(n._id);
    });

    const results = await Note.find({user: req.user._id, _id: {$in: arr}});
    if(results.length > 0){
        return res.status(400).send("Error: These notes already exist");
    }
    for(let key of req.body.notes){
        key.user = req.user._id;
        delete key._id;
    }

    const notes = await Note.insertMany(req.body.notes);

    res.json(notes);
});

router.put('/:id', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const note = await Note.updateOne({_id: req.params.id, user: req.user._id}, req.body, { new: true });
    res.json(note);
});

router.post('/delete' , async (req, res) => {
    const result = await Note.deleteMany({_id: { $in: req.body.notes}, user: req.user._id});

    if(result.deletedCount === 0) return res.status(404).send("No items matching this query");

    res.json(result);
});


module.exports = router;