//npm modules
const _ = require('lodash');
const nodemailer = require('nodemailer');
const config = require('config');
const jwt = require('jsonwebtoken');
const { Client } = require('authy-client');
const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');
const express = require('express');
const hb = require('handlebars');
const fs = require('fs');
const { promisify } = require('util');
const axios = require('axios');

//local modules
const { User, validate, validateUserChanges, validateEmail, validateNewPass } = require('../models/user');
const { Note } = require('../models/note');
const auth = require('../middleware/auth');

//setup

const readFileAsync = promisify(fs.readFile);

const router = express.Router();
const key = config.get('authy_key');
const client = new Client({ key });

//routes

router.post('/resetpassword', async (req, res) => {
    const { error } = validateEmail(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const email = req.body.email.toLowerCase();

    let user = await User.findOne({ email });
    if (!user) return res.status(404).send("A user with this email was not found.");

    const token = user.generateAuthToken(false);
    try {

        user.validResetToken = token;
        await user.save();
        let transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "apikey",
                pass: config.get('nodemailer_key')
            }
        });
        const html = await readFileAsync('./resources/template.html', { encoding: 'utf-8' });
        let template = hb.compile(html);
        let replacements = {
            url: `https://notes.saddexproductions.com/api/users/resetpassword?token=${token}&email=${email}`
        };
        let htmlSend = template(replacements);
        const mailOptions = {
            from: "noreply@saddexproductions.com", // sender address
            to: email, // list of receivers
            subject: "Reset password", // Subject line
            html: htmlSend
        }
        transporter.sendMail(mailOptions, (error, info) => {
            if (err) {
                throw (err);
            }
        });

        res.status(200).send(`A password reset email was sent to the address ${user.email}`);
    }
    catch (ex) {
        return res.status(500).send("Internal server error");
    }
});
router.post('/newpassword', async (req, res) => {
    try {
        const decrypted = jwt.verify(req.header('x-reset-token'), config.get('note_jwtPrivateKey'));
        if (decrypted.login) return res.status(403).send("This token can't be used for that operation");

        const user = await User.findOne({ email: decrypted.email });
        if (!user) return res.status(404).send("A user with this email was not found.");

        const { error } = validateNewPass(req.body);
        if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

        if (req.body.password != req.body.repeatPassword) return res.status(400).send("The passwords do not match");

        const isSame = await bcrypt.compare(req.body.password, user.password);
        if (isSame) return res.status(400).send("The new password can't be the same as the old password");

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.password, salt);
        user.validResetToken = "";
        user.passwordUpdatedOn = new Date();
        try {
            await user.save();

            res.status(200).send("Password updated successfully. Redirecting...");
        }
        catch (ex) {
            return res.status(500).send("Something went wrong");
        }
    }
    catch (ex) {
        return res.status(400).send("Invalid token");
    }

});
router.post('/', async (req, res) => {

    const { error } = validate(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const email = req.body.email.toLowerCase();

    if (req.body.password !== req.body.repeatPassword) return res.status(400).send('The passwords do not match')

    //loookup user in db

    let user = await User.findOne({ email });
    if (user) return res.status(400).send("A user with this email has already been registered");

    let cellphone;
    let authyID;

    const secretKey = config.get('SP_captchaSecretKey');

    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.header("X-Real-IP")}`;

    const aResponse = await axios.get(verifyURL);

    if (aResponse.data.success) {
        if (req.body.twoFactorAuth) {

            if (!req.body.cellphone || !req.body.countryCode) return res.status(400).send('Telephone or country ISO missing')

            //remove initial zero from cellphone number

            cellphone = removeZero(req.body.cellphone);

            try {

                authyID = await addAuthy({
                    countryCode: req.body.countryCode,
                    email,
                    phone: cellphone
                });
            }
            catch (ex) {
                return res.status(500).send("Two-factor authentication failed to setup")
            }
        }
        const salt = await bcrypt.genSalt(10);
        const password = await bcrypt.hash(req.body.password, salt);
        user = new User(
            {
                email,
                password,
                migrate: 1
            });
        if (req.body.twoFactorAuth) {
            user.twoFactorAuth = {
                cellphone,
                countryCode: req.body.countryCode,
                authyID,
                active: true
            }
        }
        await user.save();
        res.status(200).json(_.pick(user, ['_id', 'email']));
    }
    else {
        return res.status(500).send("Captcha verification failed");
    }
});

router.put('/', auth, async (req, res) => {

    let token = false;

    const { error } = validateUserChanges(req.body);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    let user = await User.findById(req.user._id);
    if (!user) return res.status(404).send("User not found");

    if (!req.body.currentPassword) return res.status(401).send("Please enter the old password");

    const validPassword = await bcrypt.compare(req.body.currentPassword, user.password);
    if (!validPassword) return res.status(400).send('Please enter the current password again, correctly');

    if (req.body.password) {
        const isSame = await bcrypt.compare(req.body.password, user.password);
        if (isSame) return res.status(400).send("The new password can't be the same as the old password");

        if (req.body.password && req.body.repeatPassword && req.body.password != req.body.repeatPassword) return res.status(400).send("The passwords do not match");

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.password, salt);
        user.passwordUpdatedOn = new Date();
        token = user.generateAuthToken(true);
    }

    let cellphone,
    removeTFA;

    if (user.twoFactorAuth && user.twoFactorAuth.active) {
        if (!req.body.twoFactorAuth && user.twoFactorAuth.active || req.body.cellphone !== user.twoFactorAuth.cellphone ||
            req.body.countryCode !== user.twoFactorAuth.countryCode) {

            await client.deleteUser({ authyId: user.twoFactorAuth.authyID });

            if (req.body.cellphone !== user.twoFactorAuth.cellphone ||
                req.body.countryCode !== user.twoFactorAuth.countryCode) {

                //remove initial zero from cellphone number

                cellphone = removeZero(req.body.cellphone);

                const { user: { id: authyId } } = await client.registerUser({
                    countryCode: req.body.countryCode,
                    email: user.email,
                    phone: cellphone
                });

                user.twoFactorAuth = {
                    countryCode: req.body.countryCode,
                    cellphone: cellphone,
                    authyID: authyId,
                    active: true
                }
            }
            else {
                removeTFA = true;
            }
        }
    }
    else {
        if (req.body.twoFactorAuth) {
            if (!req.body.cellphone || !req.body.countryCode) return res.status(400).send('Telephone or country ISO missing')

            //remove initial zero from cellphone number

            cellphone = removeZero(req.body.cellphone);

            const authyID = await addAuthy({
                countryCode: req.body.countryCode,
                email: user.email,
                phone: cellphone
            });

            user.twoFactorAuth = {
                countryCode: req.body.countryCode,
                cellphone: cellphone,
                authyID,
                active: true
            }
        }
    }

    if (req.body.migrate) user.migrate = req.body.migrate;

    user = await user.save();

    if(removeTFA) {
        user = await User.findOneAndUpdate({_id: req.user._id}, {$unset: { twoFactorAuth: ""}}, {new: true})
    }

    const objToSend = _.pick(user, ['_id', 'email', 'migrate']);

    if (user.twoFactorAuth && user.twoFactorAuth.active) {
        objToSend.twoFactorAuth = _.pick(user.twoFactorAuth, ['cellphone', 'countryCode', 'active']);
    }

    res.status(200).json({ msg: "Updated settings successfully. Redirecting...", data: objToSend, token: token });

});

router.put('/savemigrate', auth, async (req, res) => {
    const { error } = Joi.number().integer().min(0).max(2).required().validate(req.body.migrate);
    if (error) return res.status(400).send('Validation error: ' + error.details[0].message);

    const user = await User.findOne({ _id: req.user._id });
    if (!user) return res.status(404).send("User not found");

    user.migrate = req.body.migrate;

    await user.save();

    res.send(req.body.migrate.toString());
});

router.get('/resetpassword', async (req, res) => {
    const user = await User.findOne({ validResetToken: req.query.token });
    if (!user) return res.status(400).send("<script type='text/javascript'>(function(){window.location.href = '/login';})()</script>");

    try {
        const decrypted = jwt.verify(req.query.token, config.get('note_jwtPrivateKey'));
        if (decrypted.email = !req.query.email || decrypted.login) return res.status(403).send("<script type='text/javascript'>(function(){window.location.href = '/login';})()</script>");
    }
    catch (ex) {
        return res.status(400).json({ msg: "Invalid token" });
    }
    const token = user.generateAuthToken(false);

    res.status(200).send("<script type='text/javascript'>(function(){var d = new Date();d.setTime(d.getTime() + (60 * 60 * 1000));var expires = 'expires=' + d.toUTCString();" +
        "document.cookie = 'reset = " + token + ";' + expires + ';path=/';;window.location.href = '/login#reset';})()</script>");
});


//delete user and all associated notes
router.delete('/', auth, async (req, res) => {
    const user = await User.findOne({ _id: req.user._id });
    if (!user) return res.status(404).send("User not found");

    await User.deleteOne({ _id: req.user._id });
    try {
        if (user.twoFactorAuth.active) {
            //if user has 2FA enabled, delete user from Authy database
            await client.deleteUser({ authyId: user.twoFactorAuth.authyID });
        }
        await Note.deleteMany({ user: req.user._id })
    }
    catch (ex) {
        //Rollback of previous db operation if the second one fails since transactions can't be used due to MongoDB not being installed as replica set
        await User.insertOne(user);
        res.status(500).send('Something went wrong');
    }

    res.send('Deletion successful');
});

//function to remove initial zero from cellphone number
const removeZero = cellphone => {
    cellphone = cellphone.split('');
    if (cellphone[0] === "0") {
        cellphone.splice(0, 1)
    }
    return cellphone.join('');
}
//add authy user, returns Authy id on success
const addAuthy = userData => {
    return new Promise(async (res, rej) => {
        try {
            const { user: { id: authyId } } = await client.registerUser(userData);
            return res(authyId);
        }
        catch(ex) {
            return rej(ex)
        }
    });
}

module.exports = router;