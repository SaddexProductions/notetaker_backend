const auth = require('../routes/auth');
const notes = require('../routes/notes');
const users = require('../routes/users');
const bodyParser = require('body-parser');
const error = require('../middleware/errors');

module.exports = function(app){
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use('/api/login', auth);
    app.use('/api/notes', notes);
    app.use('/api/users', users);
    app.use(error);
}