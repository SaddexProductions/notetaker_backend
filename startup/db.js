const config = require('config');
const mongoose = require('mongoose');

module.exports = () => {
    mongoose.connect('mongodb://localhost/Notes', {useNewUrlParser: true, useUnifiedTopology: true,
    useCreateIndex: true, useFindAndModify: false
});
}

