const config = require('config');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const JoiPhone = require('joi-phone-number');
const Joi = require('@hapi/joi').extend(JoiPhone);

const userSchema = new mongoose.Schema({
  email: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 100,
      unique: true
  },
  password: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 1024,
  },
  twoFactorAuth: {
    cellphone: {
      type: String,
      minlength: 7,
      maxlength: 15,
      validate: /^([0-9][0-9][0-9])\W*([0-9][0-9]{2})\W*([0-9]{0,5})$/
    },
    countryCode: {
      type: String,
      minlength: 2,
      maxlength: 2
    },
    authyID: {
      type: Number
    },
    active: Boolean
  },
  migrate: {
    type: Number,
    required: true
  },
  validResetToken: String,
  passwordUpdatedOn: Date
}, {collection: 'users'});

userSchema.methods.generateAuthToken = function(login = false, twoFactor = false){
  const token = jwt.sign({
    _id: this._id,
    email: this.email,
    login: login,
    forTwoFactor: twoFactor,
    authyID: twoFactor ? this.twoFactorAuth.authyID : false,
    signedOn: new Date()
  }, config.get('note_jwtPrivateKey'));
  return token;
}

const User = mongoose.model('User', userSchema);

function validateUser(user) {
    const schema = Joi.object({
      email: Joi.string().min(5).max(100).required().email(),
      password: Joi.string().regex(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/).min(5).max(25).required(),
      repeatPassword: Joi.string().min(5).max(25).required(),
      captcha: Joi.string().required(),
      twoFactorAuth: Joi.bool(),
      cellphone: Joi.string().phoneNumber(),
      countryCode: Joi.string()
    });
  
    return schema.validate(user);
  }
  function validateUserChanges (req){
    const schema = Joi.object({
      currentPassword: Joi.string().min(5).max(25).required(),
      migrate: Joi.number().integer().min(0).max(2),
      password: Joi.string().min(5).max(25),
      twoFactorAuth: Joi.bool(),
      cellphone: Joi.string().phoneNumber(),
      repeatPassword: Joi.string().min(5).max(25),
      countryCode: Joi.string()
    });

    return schema.validate(req);
  }
  function validateEmail(req){
    const schema = Joi.object({
      email: Joi.string().min(5).max(100).required().email()
    });
    return schema.validate(req);
  }

  function validateNewPass (req){
    const schema = Joi.object({
      password: Joi.string().min(5).max(25).required(),
      repeatPassword: Joi.string().min(5).max(25).required()
    });
    return schema.validate(req);
  }

  exports.User = User;
  exports.validate = validateUser;
  exports.validateUserChanges = validateUserChanges;
  exports.validateEmail = validateEmail;
  exports.validateNewPass = validateNewPass;