//npm modules
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
Joi.ObjectId = require('joi-objectid')(Joi);

//setup
const ObjectId = mongoose.Schema.Types.ObjectId;

const noteSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 100,
        validate: /^[^\\/:\*\?"<>\|]+$/
    },
    tags: {
        type: [String]
    },
    content: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 10000
    },
    user: {
        type: ObjectId,
        required: true
    },
    date: Date,
    edited: Date
},{collection: 'notes'});

const Note = mongoose.model('Note', noteSchema);

const schema = Joi.object({
    _id: Joi.ObjectId(),
    title: Joi.string().min(2).max(100).regex(/^[^\\/:\*\?"<>\|]+$/).required(),
    tags: Joi.array().items(Joi.string().min(2).max(40).regex(/^([A-Za-z0-9\s]*)$/)).max(5),
    content: Joi.string().min(2).max(10000).required(),
    date: Joi.date(),
    edited: Joi.date()
});

function validate (note) {
    return schema.validate(note);
}

function validateArray (notes) {
    const arraySchema = Joi.array().items(schema);

    return arraySchema.validate(notes);
}

exports.Note = Note;
exports.validate = validate;
exports.validateArray = validateArray;